import org.apache.commons.codec.binary.Hex
import org.apache.commons.lang.RandomStringUtils;
import org.apache.ofbiz.base.crypto.BlowFishCrypt
import org.apache.ofbiz.base.crypto.HashCrypt
import org.apache.ofbiz.base.util.*
import org.apache.ofbiz.common.login.LoginServices
import org.apache.ofbiz.service.ModelService
import org.apache.ofbiz.service.ServiceUtil

import javax.xml.bind.DatatypeConverter
import java.nio.charset.Charset
import java.security.SecureRandom
import java.util.Map.Entry

public sendRegisterCode() {
    checkUserLogin = from("UserLogin").where([userLoginId: email]).queryOne()
    if (UtilValidate.isNotEmpty(checkUserLogin)) {
        return ServiceUtil.returnError("The email [" + email.toString() + "] already registered.")
    }
    result = ServiceUtil.returnSuccess("Register code sent to your email.");
    systemUserLogin = from("UserLogin").where([userLoginId: "system"]).queryOne();
    random = new Random( System.currentTimeMillis() )
    String trackingCode = random.nextInt(1000000)
    serviceResult = runService("createTrackingCode", [
            trackingCodeId: delegator.getNextSeqId("TrackingCode"),
            trackingCodeTypeId: "ACCESS",
            comments: email,
            description: trackingCode,
            fromDate: UtilDateTime.nowTimestamp(),
            thruDate: UtilDateTime.getTimestamp(UtilDateTime.nowTimestamp().getTime() + 60 * 60 * 1000),
            userLogin: systemUserLogin]);
    emailTemplateSetting = from("EmailTemplateSetting").where("emailTemplateSettingId", "API_REGISTER_CODE").queryOne();
    //userLogin = from("UserLogin").where("userLoginId", email).queryOne();
    serviceContext = [:]
    serviceContext.put("bodyScreenUri", emailTemplateSetting.getString("bodyScreenLocation"));
    serviceContext.put("bodyParameters", [trackingCode: trackingCode]);
    serviceContext.put("subject", emailTemplateSetting.getString("subject"));
    serviceContext.put("sendFrom", emailTemplateSetting.get("fromAddress"));
    serviceContext.put("sendTo", email);
    serviceContext.put("userLogin", systemUserLogin);
    serviceContext.put("socketFactoryClass", "javax.net.SocketFactory")
    serviceResult = runService("sendMailHiddenInLogFromScreen", serviceContext);
    return result;
}

public createAccount() {
    result = ServiceUtil.returnSuccess("Register account successful.");
    systemUserLogin = from("UserLogin").where([userLoginId: "system"]).queryOne();
    checkCode = from("TrackingCode").where(["comments": email, description: registerCode]).queryFirst()
    if (UtilValidate.isNotEmpty(checkCode)) {
        checkCode = from("TrackingCode").where(["comments": email, description: registerCode]).filterByDate().queryFirst()
        if (UtilValidate.isEmpty(checkCode)) {
            return ServiceUtil.returnError("Register code is expired.");
        }
    }
    if (!UtilValidate.isEmail(email)) {
        return result = ServiceUtil.returnError("Your email is missing.");
    }
    serviceResult = runService("createPersonAndUserLogin", [currentPassword: currentPassword, currentPasswordVerify: currentPasswordVerify, userLoginId: email,
        userLogin: systemUserLogin]);
    partyRole = runService("createPartyRole", [partyId: serviceResult.partyId, roleTypeId: "CUSTOMER", userLogin: systemUserLogin]);
    email = runService("createPartyEmailAddress", [
        emailAddress: email, partyId: serviceResult.partyId, roleTypeId: "CUSTOMER", contactMechTypeId: "EMAIL_ADDRESS",
        contactMechPurposeTypeId: "PRIMARY_EMAIL", userLogin: systemUserLogin]);
    result.userLoginId = email.toString()
    return result
}

public login() {
    result = ServiceUtil.returnSuccess();
    securityextUiLabels = UtilProperties.getResourceBundleMap("SecurityextUiLabels", locale);
    password = HashCrypt.cryptUTF8(LoginServices.getHashType(), null, password);
    userLoginResult = from("UserLogin").where("userLoginId", email).queryOne();
    if (!userLoginResult || "N".equals(userLoginResult.enabled)) {
        result = ServiceUtil.returnError(securityextUiLabels.get("loginservices.user_not_found"));
    } else {
        validPassword = HashCrypt.comparePassword(userLoginResult.get("currentPassword"), LoginServices.getHashType(), parameters.password);
        if (!validPassword) {
            result = ServiceUtil.returnError(securityextUiLabels.get("loginservices.password_incorrect"));
        } else {
            oldVisits = from("Visit").where("userLoginId", email).filterByDate().queryList();
//            for (visit in oldVisits) {
//                visit.thruDate = UtilDateTime.nowTimestamp();
//                visit.store();
//            }
            random = new SecureRandom();
            bytes = new byte[20];
            random.nextBytes(bytes);
            authToken = "ofbiz-" + Hex.encodeHexString(bytes);
            // Set role
            roleTypeId = "CUSTOMER"
            userLoginSecurityGroup = from("UserLoginSecurityGroup").where([userLoginId: email, groupId: "FULLADMIN"]).queryFirst()
            if (userLoginSecurityGroup) roleTypeId = "ADMIN"

            visit = delegator.makeValue("Visit");
            visit.set("visitId", delegator.getNextSeqId("Visit"));
            visit.set("userLoginId", userLoginResult.userLoginId);
            visit.set("partyId", userLoginResult.partyId);
            visit.set("initialRequest", "callServiceApi");
            visit.set("initialUserAgent", "API-backend");
            visit.set("cookie", authToken);
            visit.set("fromDate", UtilDateTime.nowTimestamp());
            visit.create();
            result.put("role", roleTypeId);
            result.put("authToken", authToken);
        }
    }
    return result;
}
public loginWithFacebook() {
    try {
        result = ServiceUtil.returnSuccess()
        securityextUiLabels = UtilProperties.getResourceBundleMap("SecurityextUiLabels", locale)
        userLoginResult = from("UserLogin").where("userLoginId", email).queryOne()

        if (!userLoginResult) {
            systemUserLogin = from("UserLogin").where([userLoginId: "system"]).queryOne()
            random = new SecureRandom()
            bytes = new byte[5]
            random.nextBytes(bytes)
            currentPassword = Hex.encodeHexString(bytes)
            serviceResult = runService("createPersonAndUserLogin", [
                    firstName: firstName,
                    lastName: lastName,
                    currentPassword: currentPassword,
                    currentPasswordVerify: currentPassword,
                    userLoginId: email,
                    userLogin: systemUserLogin])
            partyRole = runService("createPartyRole", [partyId: serviceResult.partyId, roleTypeId: "CUSTOMER", userLogin: systemUserLogin])
            emailResult = runService("createPartyEmailAddress", [
                    emailAddress: email, partyId: serviceResult.partyId, roleTypeId: "CUSTOMER", contactMechTypeId: "EMAIL_ADDRESS",
                    contactMechPurposeTypeId: "PRIMARY_EMAIL", userLogin: systemUserLogin])
            userLoginResult = from("UserLogin").where("userLoginId", email).queryOne()
        }
        // Set role
        roleTypeId = "CUSTOMER"
        userLoginSecurityGroup = from("UserLoginSecurityGroup").where([userLoginId: email, groupId: "FULLADMIN"]).queryFirst()
        if (userLoginSecurityGroup) roleTypeId = "ADMIN"

        visit = delegator.makeValue("Visit")
        thruDateLong = UtilDateTime.nowTimestamp().getTime() + (expiresIn * 1000)
        thruDate = UtilDateTime.getTimestamp(thruDateLong)
        visit.set("visitId", delegator.getNextSeqId("Visit"))
        visit.set("userLoginId", userLoginResult.userLoginId)
        visit.set("partyId", userLoginResult.partyId)
        visit.set("initialRequest", "callServiceApi")
        visit.set("initialUserAgent", "API-backend-facebook")
        visit.set("cookie", authToken)
        visit.set("fromDate", UtilDateTime.nowTimestamp())
        visit.set("thruDate", thruDate)
        visit.create()
        result.put("role", roleTypeId)
        result.put("authToken", authToken)
    } catch (Exception e) {
        Debug.logError(e, null)
        return ServiceUtil.returnError("Get UI Labels is missing.")
    }
    return result
}

public loginWithGooglePlus() {
    try {
        result = ServiceUtil.returnSuccess()
        securityextUiLabels = UtilProperties.getResourceBundleMap("SecurityextUiLabels", locale)
        userLoginResult = from("UserLogin").where("userLoginId", email).queryOne()

        if (!userLoginResult) {
            systemUserLogin = from("UserLogin").where([userLoginId: "system"]).queryOne()
            random = new SecureRandom()
            bytes = new byte[5]
            random.nextBytes(bytes)
            currentPassword = Hex.encodeHexString(bytes)
            serviceResult = runService("createPersonAndUserLogin", [
                    firstName: firstName,
                    lastName: lastName,
                    currentPassword: currentPassword,
                    currentPasswordVerify: currentPassword,
                    userLoginId: email,
                    userLogin: systemUserLogin])
            partyRole = runService("createPartyRole", [partyId: serviceResult.partyId, roleTypeId: "CUSTOMER", userLogin: systemUserLogin])
            emailResult = runService("createPartyEmailAddress", [
                    emailAddress: email, partyId: serviceResult.partyId, roleTypeId: "CUSTOMER", contactMechTypeId: "EMAIL_ADDRESS",
                    contactMechPurposeTypeId: "PRIMARY_EMAIL", userLogin: systemUserLogin])
            userLoginResult = from("UserLogin").where("userLoginId", email).queryOne()
        }
        // Set role
        roleTypeId = "CUSTOMER"
        userLoginSecurityGroup = from("UserLoginSecurityGroup").where([userLoginId: email, groupId: "FULLADMIN"]).queryFirst()
        if (userLoginSecurityGroup) roleTypeId = "ADMIN"

        visit = delegator.makeValue("Visit")
        thruDate = UtilDateTime.getTimestamp(expires)
        visit.set("visitId", delegator.getNextSeqId("Visit"))
        visit.set("userLoginId", userLoginResult.userLoginId)
        visit.set("partyId", userLoginResult.partyId)
        visit.set("initialRequest", "callServiceApi")
        visit.set("initialUserAgent", "API-backend-google")
        visit.set("cookie", authToken)
        visit.set("fromDate", UtilDateTime.nowTimestamp())
        visit.set("thruDate", thruDate)
        visit.create()
        result.put("role", roleTypeId)
        result.put("authToken", authToken)
    } catch (Exception e) {
        Debug.logError(e, null)
        return ServiceUtil.returnError("Get UI Labels is missing.")
    }
    return result
}

public generateApiKey() {
    result = ServiceUtil.returnSuccess()
    if (!parameters.userLoginId) {
        userLoginId = userLogin.userLoginId
    }
    userLoginResult = from("UserLogin").where([userLoginId: userLoginId]).queryOne()

    secretKey = RandomStringUtils.randomAlphanumeric(10)
    bfc = new BlowFishCrypt(secretKey.getBytes())
    apiKey = DatatypeConverter.printHexBinary(secretKey.getBytes()).toLowerCase()
    apiKeyValue = delegator.makeValue("ApiKey")
    apiKeyId = delegator.getNextSeqId("ApiKey")
    apiKeyValue.set("apiKeyId", apiKeyId)
    apiKeyValue.set("userLoginId", userLoginResult.userLoginId)
    apiKeyValue.set("apiKey", apiKey)
    apiKeyValue.set("apiSecret", secretKey)
    apiKeyValue.set("fromDate", UtilDateTime.nowTimestamp())
    apiKeyValue.create()
    result.apiKeyId = apiKeyId
    return result
}

public removeApiKey() {
    result = ServiceUtil.returnSuccess("Remove Api Key successful.")
    apiKey = from("ApiKey").where([apiKeyId: apiKeyId]).queryOne()
    apiKey.thruDate = UtilDateTime.nowTimestamp()
    apiKey.store()
    return result
}


public getApiKeyList() {
    result = ServiceUtil.returnSuccess()
    apiKeyList = from("ApiKey").where([userLoginId: userLogin.userLoginId])
            .filterByDate()
            .queryList()
    result.put("apiKeyList", apiKeyList)
    return result
}
public getApiKey() {
    result = ServiceUtil.returnSuccess()
    apiKey = from("ApiKey").where([
            userLoginId: userLogin.userLoginId, apiKeyId: apiKeyId]).queryOne()
    result.put("apiKey", apiKey.getAllFields())
    return result
}
public getApiKeyExt() {
    result = ServiceUtil.returnSuccess()
    apiKey = from("ApiKey").where([apiKey: apiKey, apiSecret: apiSecret]).queryFirst()
    result.put("apiKey", apiKey.getAllFields())
    return result
}
public changePassword() {
    Map result = ServiceUtil.returnSuccess();
    List<String> errorMessageList = new LinkedList<String>();

    checkCode = from("TrackingCode").where(["comments": email, description: recoveryCode]).queryFirst()
    if (UtilValidate.isNotEmpty(checkCode)) {
        checkCode = from("TrackingCode").where(["comments": email, description: recoveryCode]).filterByDate().queryFirst()
        if (UtilValidate.isEmpty(checkCode)) {
            return ServiceUtil.returnError("Recovery code is expired.");
        }
    }
    userLogin = from("UserLogin").where([userLoginId: email]).queryOne()
    LoginServices.checkNewPassword(userLogin, null, currentPassword, currentPasswordVerify, null, errorMessageList, true, locale);
    if (errorMessageList.size() == 0) {
        systemUserLogin = from("UserLogin").where([userLoginId: "system"]).queryOne()
        serviceResult = dispatcher.runSync("updatePassword", [
            currentPassword: currentPassword,
            newPassword: currentPassword,
            newPasswordVerify: currentPasswordVerify,
            userLoginId: email,
            userLogin: systemUserLogin])
        ModelService model = dispatcher.getDispatchContext().getModelService("api-changePassword");
        result = model.makeValid(serviceResult, ModelService.OUT_PARAM);
    } else {
        result = ServiceUtil.returnError(errorMessageList[0]);
    }
    return result;
}
public getAccount() {
    try {
        result = ServiceUtil.returnSuccess()
        user = [:]
        user.email = userLogin.userLoginId
        result.user = user
    } catch (Exception e) {
        Debug.logError(e, null);
        return ServiceUtil.returnError("Get Account is missing.")
    }
    return result;
}
public forgotPassword() {
    result = ServiceUtil.returnSuccess("Recovery code sent to your email.");
    userLogin = from("UserLogin").where("userLoginId", email).queryOne();
    if (UtilValidate.isEmpty(userLogin)) {
        return ServiceUtil.returnError("The account [" + email +"] doesn't exist.")
    }
    systemUserLogin = from("UserLogin").where([userLoginId: "system"]).queryOne();
    random = new Random( System.currentTimeMillis() )
    String trackingCode = random.nextInt(1000000)
    serviceResult = runService("createTrackingCode", [
        trackingCodeId: delegator.getNextSeqId("TrackingCode"),
        trackingCodeTypeId: "ACCESS",
        description: trackingCode,
        comments: email,
        fromDate: UtilDateTime.nowTimestamp(),
        thruDate: UtilDateTime.getTimestamp(UtilDateTime.nowTimestamp().getTime() + 5 * 60 * 1000),
        userLogin: systemUserLogin]);
    emailTemplateSetting = from("EmailTemplateSetting").where("emailTemplateSettingId", "API_FORGOT_CODE").queryOne();

    serviceContext = [:]
    serviceContext.put("bodyScreenUri", emailTemplateSetting.getString("bodyScreenLocation"));
    serviceContext.put("bodyParameters", [trackingCode: trackingCode]);
    serviceContext.put("subject", emailTemplateSetting.getString("subject"));
    serviceContext.put("sendFrom", emailTemplateSetting.get("fromAddress"));
    serviceContext.put("sendTo", email);
    serviceContext.put("partyId", userLogin.partyId);
    serviceResult = runService("sendMailHiddenInLogFromScreen", serviceContext);
    return result;
}
public logout() {
    result = ServiceUtil.returnSuccess("Logout account successful.")
    oldVisits = from("Visit").where("cookie", authorization).filterByDate().queryList()
    for (visit in oldVisits) {
        visit.thruDate = UtilDateTime.nowTimestamp()
        visit.store()
    }
    return result
}

public getUiLabels() {
    try {
        result = ServiceUtil.returnSuccess()
        InputStream is = new FileInputStream(FileUtil.getFile("component://api/config/ApiUiLabels.xml"))
        Locale locale = new Locale(language)
        apiProperties = UtilProperties.xmlToProperties(is, locale, null)
        uiLabelMap = [:]
        for(Entry<Object, Object> entry : apiProperties.entrySet()) {
            uiLabelMap.put(entry.getKey(), entry.getValue())
        }
        result.uiLabelMap = uiLabelMap
    } catch (Exception e) {
        Debug.logError(e, null);
        return ServiceUtil.returnError("Get UI Labels is missing.")
    }
    return result
}
public getNewsList() {
    try {
        // get newsList
        newsList = []
        contentList = select("contentId", "contentName", "dataResourceId", "createdDate").from("Content")
                .where(["contentTypeId": "NEWS", "localeString" : language, ownerContentId: rootId])
                .orderBy("-createdDate").queryList()
        result = ServiceUtil.returnSuccess()
        contentList.each { content ->
            text = from("ElectronicText").where([dataResourceId: content.dataResourceId]).queryOne()
            println(content)
            newsList.add([
                    contentId: content.contentId,
                    contentName: content.contentName,
                    text: text.textData,
                    createdDate: content.createdDate
            ])
        }
        result.newsList = newsList
        return result
    } catch (Exception e) {
        Debug.logError(e, null);
        return ServiceUtil.returnError("Get News List is missing.")
    }
}