package com.mymemo.api.jersey;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.ofbiz.base.util.Debug;
import org.apache.ofbiz.base.util.UtilHttp;
import org.apache.ofbiz.base.util.UtilValidate;
import org.apache.ofbiz.entity.Delegator;
import org.apache.ofbiz.entity.GenericEntityException;
import org.apache.ofbiz.entity.GenericValue;
import org.apache.ofbiz.entity.util.EntityQuery;
import org.apache.ofbiz.service.GenericServiceException;
import org.apache.ofbiz.service.LocalDispatcher;
import org.apache.ofbiz.service.ModelService;
import org.apache.ofbiz.service.ServiceUtil;

/**
 * @author Pavel Bucek (pavel.bucek at oracle.com)
 */
@Path("/")
public class JerseyCallService {
    public static final String module = JerseyCallService.class.getName();

    /**
     * Calls service
     *
     * @see "Jersey File Upload Example"
     * http://howtodoinjava.com/jersey/jersey-file-upload-example/
     *
     * @see "Example 3.13. Obtaining general map of form parameters"
     * https://jersey.java.net/documentation/latest/jaxrs-resources.html
     *
     * @param request
     * @param serviceName
     * @param formParams
     * @return
     */
    @POST
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED, MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{serviceName}")
    public Map<String, Object> callService(@Context HttpServletRequest request,
            @PathParam("serviceName") String serviceName, MultivaluedMap<String, String> formParams) {
        Map<String, Object> result = new HashMap<String, Object>();
        LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        Map<String, Object> inMap = UtilHttp.getCombinedMap(request);
        if (UtilValidate.isNotEmpty(formParams)) {
            inMap.putAll(formParams);
        }
        try {
            String service = "api-" + serviceName;
            ModelService model = dispatcher.getDispatchContext().getModelService(service);
            String authorization = request.getHeader("authorization");
            GenericValue visit = EntityQuery.use(delegator).from("Visit")
                    .where("cookie", authorization).filterByDate().cache(true).queryFirst();
            if(UtilValidate.isNotEmpty(visit) && model.auth) {
                GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin")
                        .where("userLoginId", visit.get("userLoginId")).cache(true).queryOne();
                inMap.put("userLogin", userLogin);
                inMap.put("authorization", authorization);
            }
            Map<String, Object> context = model.makeValid(inMap, ModelService.IN_PARAM);
            result = dispatcher.runSync(service, context);
        } catch (GenericServiceException e) {
            result = ServiceUtil.returnError(e.getMessage());
        } catch (GenericEntityException e) {
            result = ServiceUtil.returnError(e.getMessage());
        }
        return result;
    }

}